import 'package:flutter/material.dart' show runApp;
import 'package:panic_app/src/app.dart';

void main() => runApp(App());
