import 'package:flutter/material.dart';
import 'package:panic_app/src/screens/login/mixins/validation_mixin.dart';
import 'package:panic_app/src/screens/login/requests/login_request.dart' as req;
import 'package:panic_app/src/utils/navigate.dart';
import 'package:toast/toast.dart';
import 'package:panic_app/src/values/strings.dart' show Strings;

class InputsWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => InputsWidgetState();
}

class InputsWidgetState extends State<InputsWidget> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;

  void setEmail(String email) => this._email = email;
  void setPassword(String password) => this._password = password;

  void validateSignIn() async {
    if (this._formKey.currentState.validate()) {
      this._formKey.currentState.save();

      // connect to API
      var validated = await req.validateUser(this._email, this._password);
      validated
          ? navigateTutorial(this.context)
          : Toast.show(Strings.login.notAllowed, this.context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(color: Colors.deepPurple[600]),
      padding: EdgeInsets.only(top: 24, left: 24, right: 24),
      child: Form(
        key: this._formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            EmailFieldWidget(setEmail),
            PasswordFieldWidget(setPassword),
            ButtonWidget(validateSignIn),
          ],
        ),
      ),
    );
  }
}

// ===========================================================================
//                                CUSTOM WIDGETS
// ===========================================================================

enum EText { HINT, LABEL, INPUT }
enum EColor { ENABLE, FOCUS, ERROR }

class EmailFieldWidget extends StatelessWidget with ValidationMixin {
  final Function _setEmail;
  EmailFieldWidget(this._setEmail);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 3,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        validator: (String email) => validateEmail(email),
        onSaved: (String email) => this._setEmail(email),
        style: textFormStyle(EText.INPUT),
        decoration: InputDecoration(
          // JUST BORDER LINE
          enabledBorder: underlineBorder(EColor.ENABLE),
          focusedBorder: underlineBorder(EColor.FOCUS),
          errorBorder: underlineBorder(EColor.ERROR),
          focusedErrorBorder: underlineBorder(EColor.ERROR),
          // JUST TEXT
          hintText: Strings.login.hintEmail,
          hintStyle: textFormStyle(EText.HINT),
          labelText: Strings.login.labelEmail,
          labelStyle: textFormStyle(EText.LABEL),
        ),
      ),
    );
  }
}

class PasswordFieldWidget extends StatelessWidget with ValidationMixin {
  final Function _setPassword;
  PasswordFieldWidget(this._setPassword);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: true,
      validator: (String password) => validatePassword(password),
      onSaved: (String password) => this._setPassword(password),
      style: textFormStyle(EText.INPUT),
      decoration: InputDecoration(
        // JUST BORDER LINE
        enabledBorder: underlineBorder(EColor.ENABLE),
        focusedBorder: underlineBorder(EColor.FOCUS),
        errorBorder: underlineBorder(EColor.ERROR),
        focusedErrorBorder: underlineBorder(EColor.ERROR),
        // JUST TEXT
        labelText: Strings.login.labelPassword,
        labelStyle: textFormStyle(EText.LABEL),
      ),
    );
  }
}

class ButtonWidget extends StatelessWidget {
  final Function _validateSignIn;
  ButtonWidget(this._validateSignIn);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16.0),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        color: Colors.pink[600],
        onPressed: () => this._validateSignIn(),
        child: Text(
          Strings.login.signin,
          style: TextStyle(
            color: Colors.white.withOpacity(0.77),
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
// ===========================================================================
//                                AUX FUNCTIONS
// ===========================================================================

TextStyle textFormStyle(EText type) {
  return TextStyle(
    fontSize: 14,
    color: Colors.white.withOpacity(
        type == EText.INPUT ? 0.9 : type == EText.LABEL ? 0.67 : 0.36),
  );
}

UnderlineInputBorder underlineBorder(EColor state) {
  return UnderlineInputBorder(
    borderSide: BorderSide(
      color: (state == EColor.ENABLE
          ? Colors.pink
          : state == EColor.FOCUS
              ? Colors.pink[400]
              : state == EColor.ERROR ? Colors.red[600] : Colors.grey[700]),
    ),
  );
}
