import 'package:http/http.dart' as http;
import 'dart:convert';

Future<bool> validateUser(String email, String password) async {
  var url = "https://open-pucp-panic-button.herokuapp.com/api/login";
  var headers = {'Content-Type': 'application/json'};
  var body = '{"email": "$email", "password": "$password"}';
  print(body);
  var res = await http.post(url, headers: headers, body: body);
  print(res.body);
  if (res.statusCode == 200) {
    var response = Response.fromJson(json.decode(res.body));
    print(response.ok);
    return response.ok;
  }

  return false;
}

class Response {
  String _status;
  int _code;
  String _message;

  get ok => this._code == 200;

  Response(this._status, this._code, this._message);

  factory Response.fromJson(json) {
    return Response(json['status'], json['code'], json['message']);
  }
}
