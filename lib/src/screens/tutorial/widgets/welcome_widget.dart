import 'package:flutter/material.dart';
import 'package:panic_app/src/values/assets.dart' show Assets;
import 'package:panic_app/src/values/strings.dart' show Strings;

class WelcomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      padding: EdgeInsets.only(top: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(height: MediaQuery.of(context).size.height * 0.05),
          ImageWidget(),
          TextWidget(),
          Container(height: MediaQuery.of(context).size.height * 0.025),
          LineWidget(),
        ],
      ),
    );
  }
}

class ImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      width: MediaQuery.of(context).size.height * 0.25,
      padding: EdgeInsets.symmetric(vertical: 24.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white70, width: 2.0),
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage(Assets.images.user),
        ),
      ),
    );
  }
}

class TextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1,
      padding: EdgeInsets.only(top: 16.0),
      child: Text(
        Strings.tutorial.welcome,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontStyle: FontStyle.normal,
          fontSize: 16.0,
        ),
      ),
    );
  }
}

class LineWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.025,
      // height: 15sp,
      padding: EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.height * 0.011,
        horizontal: 50.0,
      ),
      child: Container(color: Colors.purple[700]),
    );
  }
}
